## Author: https://medium.com/@niiiixd/install-harbor-docker-registry-92fb13cec364

sudo yum install -y yum-utils lvm2 epel-release    
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo  

sudo yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin   
sudo usermod -aG docker $USER  
sudo systemctl start docker   
sudo systemctl enable docker  
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose   
sudo chmod +x /usr/local/bin/docker-compose    
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose    
