#!/bin/env python3
# lib
import  json
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import  InventoryManager
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import  TaskQueueManager
from ansible.plugins.callback import CallbackBase
from ansible.utils.sentinel import Sentinel

Options = namedtuple('Options',['connection', 'module_path','forks','become','become_method','become_user','check','diff'])

# ini object
loader = DataLoader()
options = Options(connection='local', module_path='',forks='20',become='None',become_method='None',become_user='None', check='True', diff='Flase')
passwords = dict(vault_pass='1234')

# create inventory
inventory = InventoryManager(loader=loader, sources=['./Notes-Ansible/hosts'])
variable_manager = VariableManager(loader=loader, inventory=inventory)

# create play
play_source = dict(
    name = "check python version",
    hosts ="server",
    gather_facts = "no",
    tasks = [
        dict(action = dict(module = 'shell',  args = 'hostname'), register = 'shell_out'), 
        dict(action = dict(module = 'debug',  args = dict(msg = '{{shell_out.stdout}}')))
    ]
)

play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

# execute
task = None
try:
    task = TaskQueueManager(
        inventory = inventory,
        variable_manager = variable_manager,
        loader = loader,
        # options = options,
        passwords = passwords,
        stdout_callback = 'default'
    )
    result = task.run(play)
finally:
    if task is not None:
        task.cleanup()



from ansible.module_utils.common.collections import ImmutableDict
# 注意: 使用namedtuple實現Options的方式，官網已經不推薦使用
#       網上很多例子都是按照Options的方式，會有報錯如下：
# error_msg: "msg": "the connection plugin '<class 'ansible.utils.sentinel.Sentinel'>' was not found",
# 網上提供解決方案： https://blog.csdn.net/FMT21/article/details/103468284
# 但是目前驗證報錯，options 最終的tuple類型，在初始化的時候用到了 vars 系統函數，但是該函數報錯
# TypeError: vars() argument must have __dict__ attribute
# from collections import namedtuple
# Options = namedtuple('Options', ['connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'check', 'diff'])
# options = Options(connection='ssh', module_path=['/to/mymodules'], forks=10, become=None, become_method=None, become_user=None, check=False, diff=False)