# Python NOTES

Book: Python技術手冊3.7, 王者歸來, 精通 Python 1e
Env: Ubuntu 20.04, Windows server 20XX


## Python PATH
Python Interpreter會根據python path尋找模組，比如現在不透過pip直接git clone下載python加密套件cryptography就可以test1_cypto.py中使用對稱加密演算法加密訊息
```
export PYTHONPATH=$PYTHONPATH:$HOME/workspace/python  #set env var , default in workspace floder
env  | grep PYTHONPATH
git clone https://github.com/pyca/cryptography.git
sleep 2

python3  ./test1_cypto.py
```     

## WHY Ops & DevOps need learn Python
為何要學Python，比如像從Linux開始的工程師就會覺得windows全家桶真奇怪還分CMD與PowerShell，重點以前保護壁壘思想影響下Windows很多邏輯跟Linux不一樣，最經典的就是跳脫符號, 反斜線與換行，而用Python就可以打破這層壁壘，尤其現代的DevOps風潮下(當然問10個人DevOps的定義會有11種答案)，而小弟的定義就是照字面翻: 用Dev當方法做Ops，那就需要一種能輕易RUN在不同環境 (Win/Linux Server, Network/Embed Device)的Ops語言。

當然也可以自虐用C++做Ops拉!不僅Win是用C++寫的，Linux底下很多知名軟體Tensorflow, NGINX, MongoDB, MariaDB, MySQL主要也是用C++寫的。

## First-class function
Python中沒有template進行抽象化將函式重覆利用，但在Python中函式屬於一等公民可以當作值來傳遞實現重覆利用現在有一個需求判斷list中的字串長度是否大於5,6,7，可以寫三個if else或是寫三個function但不管哪種方式都一點不clean，雖然醜且直接的寫法有許多好處但GitHub在看很多大神的source code會很難接軌，下面是Python技術手冊的範例透過將函式當變數傳遞實現更簡潔代碼filter_demo2.py


```
def filter_gt( fun_predicate, list_str):
    res = []
    for elem in lt:
        if fun_predicate(elem):
            res.append(elem)
    return res

def len_greater_than(num):
    len_greater_than_num(elem):
        return len(elem) > num
    return len_greater_than_num

list_str =['AAA','BBBB','CCCCC','DDDDDD']
print(filter_gt(len_greater_than(5), list_str))
print(filter_gt(len_greater_than(6), list_str))
print(filter_gt(len_greater_than(7), list_str))

```

## Decorator
再來看一個例子定義一個裝飾函數裡面有包裝這個動作，任何被丟進來的任何函數都會經過裝飾函數再包裝，本來any_function()只有一個print(Second Message)，經過a_decorator_ function ()裝飾後得到另外兩個print()，print("First : before wrapper")與print("Third : after wrapper")

```
def a_decorator_function(any_function):
    print("First : before wrapper")
    any_function()
    def wrapper():
        print("Third : after wrapper")
    return wrapper

def test():
    print("Second Message)")
    

foo = a_decorator_function(test)
foo()

```
接著我們可以用裝飾器@把寫法改得更簡潔
```
def a_decorator_function(any_function):
    print("First : before wrapper")
    any_function()
    def wrapper():
        print("Third : after wrapper")
    return wrapper
    
@a_decorator_function
def demo2():
    print("Second Message)")
    
demo2()
```





## 格式化字串處理
格式化字串處理
資訊處理是Ops的核心在Linux底下有grep, awk, sed, echo, cat, netcat, printf，Python底下有re, printf, format, split ,socket當然也可以直接用subprecess把Linux與Win的系統指令塞到Python中，比如說控制Linux Demon的systemctl，現在嘗試用python去幫ubuntu安裝firewalld服務並啟動(ubuntu預設使用ufw，當然在實務上有更好方法例如使用Ansible, Chef)，這時候會需要密碼剛好前面有講到的Python加密模組當然還可以加鹽
