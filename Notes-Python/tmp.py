def a_decorator_function(any_function):
    print("First : before wrapper")
    any_function()
    def wrapper():
        print("Third : after wrapper")
    return wrapper

def demo1():
    print("Second")
    

x = a_decorator_function(demo1)

print(x)
    