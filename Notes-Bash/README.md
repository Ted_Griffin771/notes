```
#!/bin/bash             # use bash as shell
set -euo pipefail       # exit if script fails
export dns="8.8.8.8"    # set env var
declare -i sum=100+300

#小括號包起來一段指令再用$字號轉成質賦予變數
str='Linux Kernel is'
tmp=$(echo $str && uname -r; whoami)    

cat > /var/log/test.log << EOF
shell id is $$
it is int type $sum 
$tmp
EOF

cat /var/log/test.log | while read line      
do
   echo ": $line"
done

#中括號用在測試
if  [[ -d /var/log ]]         
then
    echo "It is dir"
else
    echo "..."
fi

```


```
cmd1 && cmd2
1. 若 cmd1 執行完畢且正確執行($?=0)，則開始執行 cmd2。
2. 若 cmd1 執行完畢且為錯誤 ($?≠0)，則 cmd2 不執行。

cmd1 || cmd2
1. 若 cmd1 執行完畢且正確執行($?=0)，則 cmd2 不執行。
2. 若 cmd1 執行完畢且為錯誤 ($?≠0)，則開始執行 cmd2。

sync1; sync2; cmd1;
1.同步執行sync1 sync2後才執行cmd1

cmd1 & 
cmd2 &
1.將指令變成背景下執行
```